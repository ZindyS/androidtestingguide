package com.example.testingproject

import com.google.common.truth.Truth.assertThat
import org.junit.Before
import org.junit.Test

class GoogleUnitTest {
    var ans=0
    @Before
    fun counting() {
        for (i in 0..99999) {
            ans += (2 * i) - 5 * (i + ans)
        }
    }

    @Test
    fun loop_Test() {
        assertThat(ans).isEqualTo(1202530843)
    }

    @Test
    fun div_Test() {
        assertThat(ans/2).isEqualTo(601265421)
    }
}