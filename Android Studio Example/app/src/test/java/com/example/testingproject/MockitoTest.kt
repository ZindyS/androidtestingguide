package com.example.testingproject

import android.content.Context
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class ExampleClass {
    private var ans=0

    fun getAns(): Int {
        return ans
    }

    fun setAns(a: Int, b: Int) {
        //Представим, что
        //здесь очень сложные
        //вычисления
        ans=(a+b)%10
    }
}

class GetMeMockitoClass {
    fun getAnsFromExampleClass(a: ExampleClass): Int {
        return a.getAns()*100500
    }
}

class MockitoTest {

    @Before
    fun init() {
        MockitoAnnotations.openMocks(this)
    }

    @Mock
    lateinit var mockContext: Context

    @Test
    fun mockito_Test() {
        Mockito.`when`(mockContext.getString(R.string.app_name)).thenReturn("FakeName")
        Assert.assertEquals("FakeName", mockContext.getString(R.string.app_name))
    }

    @Test
    fun mockito_Test2() {
        Mockito.`when`(mockContext.getString(Mockito.anyInt())).thenReturn("FakeName")
        Assert.assertEquals("FakeName", mockContext.getString(R.string.app_name))
        Assert.assertEquals("FakeName", mockContext.getString(R.string.appbar_scrolling_view_behavior))
        Assert.assertEquals("FakeName", mockContext.getString(R.string.title_activity_main2))
    }

    @Mock
    lateinit var mockClass: ExampleClass

    @Test
    fun mockitoClass_Test() {
        Mockito.`when`(mockClass.getAns()).thenReturn(2)
        Assert.assertEquals(201000, GetMeMockitoClass().getAnsFromExampleClass(mockClass))
    }
}