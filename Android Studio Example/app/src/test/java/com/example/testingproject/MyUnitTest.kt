package com.example.testingproject

import org.junit.Test
import org.junit.Assert.*
import org.junit.Before


class MyUnitTest {
    var ans=0
    @Before
    fun counting() {
        for (i in 0..99999) {
            ans+=(2*i)-5*(i+ans)
        }
    }

    @Test
    fun loop_Test() {
        assertEquals(1202530843, ans)
    }

    @Test
    fun div_Test() {
        assertEquals(601265421, ans/2)
    }
}