package com.example.testingproject

import org.junit.Test
import org.junit.Assert.*
import kotlin.NullPointerException

class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }

    @Test
    fun fun_isCorrect() {
        assertEquals(4, sumAandB(2, 2))
    }

    @Test(expected = NullPointerException::class)
    fun nullString_Test() {
        val str: String? = null
        assertTrue(str!!.isEmpty())
    }
}