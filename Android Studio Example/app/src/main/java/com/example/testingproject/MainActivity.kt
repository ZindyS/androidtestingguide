package com.example.testingproject

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View

fun sumAandB(a:Int, b:Int) : Int {
    return a+b
}

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun onTextClicked(v: View) {
        val intent = Intent(this, NewActivity::class.java)
        startActivity(intent)
    }
}