package com.example.testingproject

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MyUITest {
    @Rule
    @JvmField
    var mActivityTestRule = ActivityTestRule(NewActivity::class.java)

    @Test
    fun myTest() {
        onView(ViewMatchers.withId(R.id.text_home))
            .check(ViewAssertions.matches(ViewMatchers.withText("abacaba")))
    }
}