# **AndroidTestingGuide**

Как создавать тесты в **Android Studio** на **Kotlin**

Тесты – это методы, проверяющие работоспособность приложения. Мы разберём и напишем на языке ***Kotlin*** **Unit** и **UI** тестирование.

# **Unit Test**

Данные тесты существуют для проверки работоспособности кода, функций и т.д. Для такого тестирования существуют множество разных библиотек, о которых мы сегодня и поговорим.

## [**JUNIT**](https://github.com/mannodermaus/android-junit5)

Первая библиотека – **JUnit**. Она подключается автоматически при создании проекта и содержит в себе класс Assert, который позволяет создавать простейшие тесты. При создании проекта сразу генерируется в папке *com.example.<название проекта> (test)*

```kotlin
@Test
fun addition_isCorrect() {
    assertEquals(4, 2 + 2)
}
```

Этот тест делает обычную проверку совпадения ожидаемого ответа с получившимся (ожидал 4, получил 4 в ходе сложения двух двоек). Так как значения совпали, то тест считается пройденным.

Так же можно создать свой собственный тест, написав функцию и пометив её с помощью \@Test. Пусть в ***MainActivity*** (вне класса) лежит функция сложения, которую мы хотим проверить

```kotlin
fun sumAandB(a:Int, b:Int) : Int {
    return a+b
}
```

Далее напишем код, проверяющий работоспособность данной функции

```kotlin
@Test
fun fun_isCorrect() {
    assertEquals(4, sumAandB(2, 2))
}
```

Запустим наш тест.

Если тест был пройден, то будет выведено вот такое сообщение

<!--<img src="https://gitlab.com/Kavrese/androidtestingguide/-/raw/main/Photos/one.png" />-->
<img src="https://gitlab.com/ZindyS/androidtestingguide/-/raw/main/Photos/one.png"/>

Иначе будет выведено следующее

<!--<img src="https://gitlab.com/Kavrese/androidtestingguide/-/raw/main/Photos/two.png" />-->
<img src="https://gitlab.com/ZindyS/androidtestingguide/-/raw/main/Photos/two.png"/>

Помимо запуска обычных функций, можно самому писать и запускать целый класс, содержащий в себе тесты.

```kotlin
class MyUnitTest {
    var ans=0

    @Test
    fun loop_Test() {
        for (i in 0..99999) {
            ans+=(2*i)-5*(i+ans)
        }
        assertEquals(1202530843, ans)
    }

    @Test
    fun div_Test() {
        assertEquals(601265421, ans/2)
    }
}
```

Однако в примере второй тест не будет пройден, потому что сейчас все тесты выполняются “одновременно”, независимо друг от друга, поэтому в переменной ans на момент выполнения второго теста лежит 0, а не посчитанное в ***loop_Test*** значение.

Чтобы указать, что до всех тестов идут какие-то  вычисления, нужно добавить аннотацию \@Before. Тогда наш класс проходит все тесты и выглядит вот так

```kotlin
class MyUnitTest {
    var ans=0
    @Before
    fun counting() {
        for (i in 0..99999) {
            ans+=(2*i)-5*(i+ans)
        }
    }

    @Test
    fun loop_Test() {
        assertEquals(1202530843, ans)
    }

    @Test
    fun div_Test() {
        assertEquals(601265421, ans/2)
    }
}
```

Помимо \@Before есть аннотация \@After. Она указывает, что тест, помеченный данной аннотацией будет выполняться после КАЖДОГО теста.

Помимо обычных проверок можно предусматривать исключения.

```kotlin
@Test(expected = NullPointerException::class)
fun nullString_Test() {
    val str: String? = null
    assertTrue(str!!.isEmpty())
}
```

Здесь мы предусматриваем, что в строке будет значение *null*, и если в тесте будет проверка этой строки или какая-то обработка её, то тест не будет провален (хотя будет вызвана ошибка).

## [**GoogleTruth**](https://github.com/google/truth)

Далее поговорим про библиотеку **GoogleTruth**. Она по функционалу очень схожа с **JUnit**, подключается с помощью строчки  
*testImplementation ‘com.google.truth:truth:1.1.3’* в build.gradle. Эта библиотека позволяет писать код, более приближенный к английскому языку. Таким образом, наш код с использованием этой библиотеки приобретает следующий вид

```kotlin
class GoogleUnitTest {
    var ans=0
    @Before
    fun counting() {
        for (i in 0..99999) {
            ans += (2 * i) - 5 * (i + ans)
        }
    }

    @Test
    fun loop_Test() {
        assertThat(ans).isEqualTo(1202530843)
    }

    @Test
    fun div_Test() {
        assertThat(ans/2).isEqualTo(601265421)
    }
}
```

Можно заметить, что проверка осуществляется с помощью функций *assertThat()*.

## [**Mockito**](https://github.com/mockito/mockito)

Теперь поработаем с библиотекой **Mockito**, подключаемой с помощью зависимости  
*testImplementation 'org.mockito.kotlin:mockito-kotlin:4.0.0'* и  
*testImplementation 'org.mockito:mockito-inline:3.11.2'*. Класс **Mockito** позволяет создавать “заглушки”, или, как их чаще называют, моки. Моки, в свою очередь, позволяют использовать для проверки сложные классы, имитируя работу других классов.

Для начала роботы с **Mockito** нужно в классе в функцию с аннотацией \@Before поместить строчку *MockitoAnnotations.openMocks(this)*. Теперь мы можем создавать “заглушки” с помощью аннотации \@Mock. Приведу простой пример работы с моком: можно “заменить” одно значение другим

```kotlin
@Before
fun init() {
    MockitoAnnotations.openMocks(this)
}

@Mock
lateinit var mockContext: Context

@Test
fun mockito_Test() {
    Mockito.`when`(mockContext.getString(R.string.app_name)).thenReturn("FakeName")
    Assert.assertEquals("FakeName", mockContext.getString(R.string.app_name))
}
```

В этом примере я создал мок на основе класса *Context* и далее прописал логику вызова функции *getString()*. Я указал, что при вызове *getString(R.string.app_name)* нужно вернуть значение “FakeName”. Таким образом, я указал, что мой мок-контекст, хоть он фактически ничего из себя не представляет, при вызове определённой команды вызывал определённые значения.

Так же мы можем указать, что *getString()* может принимать любые значения с помощью *Mockito.anyInt()* (не забываем, что указатели на все строки у нас осуществляются с помощью целых чисел)

```kotlin
@Test
fun mockito_Test() {
    Mockito.`when`(mockContext.getString(Mockito.anyInt())).thenReturn("FakeName")
    Assert.assertEquals("FakeName", mockContext.getString(R.string.app_name))
    Assert.assertEquals("FakeName",mockContext.getString(R.string.appbar_scrolling_view_behavior))
    Assert.assertEquals("FakeName", mockContext.getString(R.string.title_activity_main2))
}
```

Для иллюстрации нового примера я создал два класса, причём одному из них для вычисления нужной нам функции принимает на вход объект другого класса

```kotlin
class ExampleClass {
    private var ans=0

    fun getAns(): Int {
        return ans
    }

    fun setAns(a: Int, b: Int) {
        //Представим, что здесь очень
        //сложные вычисления
        ans=(a+b)%10
    }
}

class GetMeMockitoClass {
    fun getAnsFromExampleClass(a: ExampleClass): Int {
        return a.getAns()*100500
    }
}
```

Если мы напрямую создадим один объект, совершим все настройки, потом передадим этот объект в нужный нам класс и проверим вернувшийся нам ответ, то это будет долго, неудобно и некрасиво. Намного приятнее сделать “заглушку”

```kotlin
@Mock
lateinit var mockClass: ExampleClass

@Test
fun mockitoClass_Test() {
    Mockito.`when`(mockClass.getAns()).thenReturn(2)
    Assert.assertEquals(201000, GetMeMockitoClass().getAnsFromExampleClass(mockClass))
}
```

Мы сделали мок-объект нашего первого класса и сказали, что если у него будут запрашивать функцию *getAns()*, то он просто вернёт 2. В итоге мы пропускаем шаг вычисления ответа и подбора переменных a и b, чтобы наш ans стал равен 2 (это мы ещё не рассмотрели случай, когда функция подсчёта ответа так же ссылается на другие классы).

Библиотека **Mockito** – очень обширная, и поэтому я не буду сейчас описывать весь её обширный функционал. Я лишь оставлю [здесь](https://habr.com/ru/post/444982/) ссылку на статью с подробным использованием моков.

# **UI Test**

Эти тесты предназначены для проверки интерфейса приложения. Если кратко, то мы имитируем действия пользователя для стабильного тестирования компонентов приложения.

## [Espresso](https://github.com/Apress/android-espresso-revealed)

Основная библиотека, которую мы будем использовать, – **Espresso**. Все UI-тесты хранятся в папке *com.example.<название проекта> (androidTest)*, где уже лежит ничем не примечательный стандартный тест.

Самый простой способ создать свой тест – использовать встроенную в **Android Studio** функцию *Record Espresso Test*, которая находится во вкладке *Run*. При использовании этой функции запустится эмулятор и откроется специальное окно

<!--<img src="https://gitlab.com/Kavrese/androidtestingguide/-/raw/main/Photos/three.png" />-->
<img src="https://gitlab.com/ZindyS/androidtestingguide/-/raw/main/Photos/three.png"/>

Если на эмуляторе в своём приложении вы будете делать определённые действия (например, нажимать на кнопки), то эти действия будут “записаны”, и при запуске этого теста они так же будут обрабатываться, как если бы эти действия сделали вы вручную. Далее можно нажать на кнопку *Add Assertion*, которая позволяет добавлять проверки элементов на экране.

<!--<img src="https://gitlab.com/Kavrese/androidtestingguide/-/raw/main/Photos/four.png" />-->
<img src="https://gitlab.com/ZindyS/androidtestingguide/-/raw/main/Photos/four.png"/>

Я,  например, поставлю в поле *Select an element from screenshot* поставлю *android.widget.TextView* (в моём случае – это AppBar, нужно ориентироваться по дереву наследия чтобы понять, что именно это за элемент). Далее можно поставить саму проверку. Я поставлю соответствие текста внутри *TextView* строке “Home”. После сохранения данного теста мы задаём название нашему тесту , и через некоторое время видим в нашей папке новый тест, который сгенерировался на основе того, что мы делали

```kotlin
@LargeTest
@RunWith(AndroidJUnit4::class)
class RecordedEspressoTest {

    @Rule
    @JvmField
    var mActivityTestRule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun recordedEspressoTest() {
        val materialTextView = onView(
            allOf(
                withText("Hello World!"),
                childAtPosition(
                    childAtPosition(
                        withId(android.R.id.content),
                        0
                    ),
                    0
                ),
                isDisplayed()
            )
        )
        materialTextView.perform(click())

        val textView = onView(
            allOf(
                withText("Home"),
                withParent(
                    allOf(
                        withId(R.id.action_bar),
                        withParent(withId(R.id.action_bar_container))
                    )
                ),
                isDisplayed()
            )
        )
        textView.check(matches(withText("Home")))
    }

    private fun childAtPosition(parentMatcher: Matcher<View>, position: Int): Matcher<View> {

        return object : TypeSafeMatcher<View>() {
            override fun describeTo(description: Description) {
                description.appendText("Child at position $position in parent ")
                parentMatcher.describeTo(description)
            }

            public override fun matchesSafely(view: View): Boolean {
                val parent = view.parent
                return parent is ViewGroup && parentMatcher.matches(parent)
                        && view == parent.getChildAt(position)
            }
        }
    }
}
```

Однако, этот тест записался от самого старта экрана до того экрана, на котором мы остановились. А что, если мы хотим проверить конкретный экран? Для этого нужно написать свой класс.

Первым делом, аннотациями \@Rule и \@JvmField обозначим “открытие” нужного нам экрана

```kotlin
@Rule
@JvmField
var mActivityTestRule = ActivityTestRule(NewActivity::class.java)
```

Давайте добавим *TextView* в ***fragment_home.xml***

```xml
<?xml version="1.0" encoding="utf-8"?>
<androidx.constraintlayout.widget.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".ui.home.HomeFragment">

    <TextView
        android:id="@+id/text_home"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="abacaba"
        android:textAlignment="center"
        android:textSize="20sp"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toTopOf="parent" />
</androidx.constraintlayout.widget.ConstraintLayout>
```

Далее напишем функцию, проверяющую текст в фрагменте HomeFragment

```kotlin
@Test
fun myTest() {
    onView(ViewMatchers.withId(R.id.text_home))
        .check(ViewAssertions.matches(ViewMatchers.withText("abacaba")))
}
```

Класс **ViewMatchers** имеет огромное количество проверяющих функций, например: *isDisplayed()*, *hasFocus()*, *isChecked()* и так далее (полный список методов можно найти [тут](https://developer.android.com/reference/androidx/test/espresso/matcher/ViewMatchers)). Таким образом можно очень подробно проверять элемент и прослеживать его действия и свойства, что очень сильно упрощает тестирование.

# **Интересные ссылки**

https://developer.android.com/studio/test - официальная документация про тестирование  
https://stfalcon.com/ru/blog/post/simple-unit-tests-for-android - разбор простых Unit-Test’ов  
https://stackoverflow.com/questions/16586409/how-can-i-create-tests-in-android-studio - очень подробно о создании Unit и UI тестов  
https://habr.com/ru/post/352334/ - чуть менее подробный разбор UI и Unit тестирования  
https://www.fandroid.info/lecture-6-on-the-architecture-of-android-unit-testing-test-driven-development/ - тут есть пример использования Unit-Test’ов с запросами  
https://www.journaldev.com/22674/android-unit-testing-junit4 - здесь более подробно показано, как можно использовать Unit-тесты при обычной разработке мобильных приложений  
https://habr.com/ru/post/444982/ - разбор Mockito  
https://stackoverflow.com/questions/68840524/mockito-cannot-mock-spy-because-final-class - здесь я нашёл решение проблемы моков с final классами  
https://developer.android.com/studio/test/espresso-test-recorder?utm_source=android-studio – официальная документация про Espresso Test Recorder  
https://developer.android.com/reference/androidx/test/espresso/matcher/ViewMatchers - все методы ViewMatchers
